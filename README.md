# Panorama Image Web Resolution Library

⭐ 用于头戴式 VR 眼镜的四种全景图像网页解析库 ⭐

## 🤔 这是什么

这是一套用于头戴式 VR 眼睛的全景图像网页解析库，可用于任何网页、客户端渲染显示全景图像。

提供了四种全景图像的渲染方式，可以通过 API 接口便捷地调用任何方式。

## 📜 开源许可

基于 [MIT License](https://choosealicense.com/licenses/mit/) 许可进行开源。
